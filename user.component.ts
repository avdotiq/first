import { Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, ValidatorFn} from '@angular/forms';
import { ERROR_MESSAGES } from '@app/config/error-messages.config';
import { noWhitespaceValidator } from '@app/services/validators';
import { IOrganization, IRole } from '@app/models/organization.models';
import { OrganizationService } from '../organizations/services/organization.service';
import { AuthService } from '@app/core/services/auth.service';
import { MessageService } from '@app/core/services/message.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'erp-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {

  @ViewChild('drawer') public sidenav;

  public organizations: IOrganization[] = [];
  public AddUser: FormGroup;
  public ERROR_MESSAGES = ERROR_MESSAGES;
  public hide = true;
  public selected = '';
  public availableCountries: string[] = ['Беларусь', 'Россия', 'Украина'];
  public userPhones: FormArray;
  public userEmails: FormArray;
  public roleVal = '';
  public showAdress: boolean;
  public availableRoles: IRole[] = [];

  private destroy$ = new Subject();

    constructor(
        private fb: FormBuilder,
        private orgServise: OrganizationService,
        private authService: AuthService,
        private messageService: MessageService
    ) {}

  get phonesUserControls(): FormArray {
    return this.AddUser.get('contact').get('phones') as FormArray;
  }

  get emailsUserControls(): FormArray {
    return this.AddUser.get('contact').get('emails') as FormArray;
  }

  ngOnInit(): void {

    this.AddUser = this.fb.group({
      userRole: this.fb.group ({
      role: [''],
      organization: ['']
      }),
      name: this.fb.group({
      first: ['', [ Validators.required, Validators.minLength(2), noWhitespaceValidator]],
      last: ['', [Validators.required, Validators.minLength(2), noWhitespaceValidator]],
      middle: ['']
      }),
      contact: this.fb.group({
      phones: this.fb.array([]),
      emails: this.fb.array([]),
      password: ['', [Validators.required, Validators.minLength(6)]]
      }),
      role: ['', Validators.required],
      actualAddress: this.fb.group ({
        country: [''],
        city: [''],
        street: [''],
        streetNumber: [''],
        zip: ['']
      })
    });

    this.AddUser.get('userRole').get('role')
        .valueChanges
        .pipe(takeUntil(this.destroy$))
        .subscribe(res => {
            this.roleVal = res;
            const organization = this.AddUser.get('userRole').get('organization');
            const country = this.AddUser.get('actualAddress').get('country');
            const city = this.AddUser.get('actualAddress').get('city');
            const street = this.AddUser.get('actualAddress').get('street');
            const streetNumber = this.AddUser.get('actualAddress').get('streetNumber');
            const adressValidators: ValidatorFn[] = [
                Validators.required,
                Validators.minLength(1)
            ];
            const role = this.AddUser.get('role');
            if (this.roleVal === 'admin') {
                this.showAdress = true;
                organization.setValidators(adressValidators);
                country.setValidators(adressValidators);
                city.setValidators(adressValidators);
                street.setValidators(adressValidators);
                streetNumber.setValidators(adressValidators);
                role.setValidators(Validators.required);
            } else {
                this.showAdress = false;
                organization.clearValidators();
                country.clearValidators();
                city.clearValidators();
                street.clearValidators();
                streetNumber.clearValidators();
                role.clearValidators();
            }
            organization.updateValueAndValidity();
            country.updateValueAndValidity();
            city.updateValueAndValidity();
            street.updateValueAndValidity();
            streetNumber.updateValueAndValidity();
            role.updateValueAndValidity();
        });

    this.AddUser.get('userRole').get('organization')
        .valueChanges
        .pipe(takeUntil(this.destroy$))
        .subscribe(orgId => {
            if (orgId) {
                this.getRole(orgId);
            }
        }
    );

    this.orgServise.getOrganizationsAll()
        .pipe(takeUntil(this.destroy$))
        .subscribe(res => {
            this.organizations = res;
        });

    this.initUserPhonesArray();
    this.initUserEmailsArray();

  }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private getRole(orgId) {
        this.orgServise.getRoles(orgId)
        .pipe(takeUntil(this.destroy$))
        .subscribe(
            roles => this.availableRoles = roles,
            error => this.messageService.showMessage('error', error)
        );
    }

  private initUserEmailsArray() {
    this.userEmails = this.AddUser.get('contact').get('emails') as FormArray;
    return this.userEmails.push(this.createUserEmailControl());
  }

  private initUserPhonesArray() {
    this.userPhones = this.AddUser.get('contact').get('phones') as FormArray;
    return this.userPhones.push(this.createUserPhoneControl());
  }

  private createUserEmailControl(email?: string): FormGroup {
    return this.fb.group({
      email: [email || '', [Validators.email, Validators.required]]
    });
  }

  private createUserPhoneControl(phone?: string): FormGroup {
    return this.fb.group({
        phone: [phone || '', [Validators.required]]
    });
  }

  addUserPhone(): void {
    this.userPhones = this.AddUser.get('contact').get('phones') as FormArray;
    this.userPhones.push(this.createUserPhoneControl(null));
  }

  removeUserPhone(index: number): void {
    this.userPhones.removeAt(index);
  }

  addUserEmail(): void {
    this.userEmails = this.AddUser.get('contact').get('emails') as FormArray;
    this.userEmails.push(this.createUserEmailControl(null));
  }

  removeUserEmail(index: number): void {
    this.userEmails.removeAt(index);
  }


  save() {
    const data = this.AddUser.value;
    const phones = data.contact.phones.map(item => item.phone && item.phone.indexOf('+375') === -1 ? '+375' + item.phone : item.phone);
    const emails = data.contact.emails.map(item => item.email);
    if (data.userRole.role === 'super_admin') {
      const user = {
        type: 'super_admin',
        name: data.name,
        phone: phones[0],
        password: data.contact.password
      };
      this.authService. registerUser(user)
      .pipe(takeUntil(this.destroy$))
      .subscribe(next => {
        this.messageService.showMessage('success', 'Сотрудник успешно добавлен!');
        this.AddUser.reset();
        this.sidenav.close();
      }, error => this.messageService.showMessage('error', error));
    } else {
      const userAdmin = {
        type: 'admin',
        role: data.role,
        roleId: data.role,
        name: data.name,
        actualAddress: data.actualAddress,
        email: emails[0],
        phones,
        organization: data.userRole.organization,
        password: data.contact.password
      };
      this.orgServise.saveStaff(userAdmin)
      .pipe(takeUntil(this.destroy$))
      .subscribe (res => {
        this.messageService.showMessage('success', 'Сотрудник успешно добавлен!');
        this.AddUser.reset();
        this.sidenav.close();
      }, error => this.messageService.showMessage('error', error));
    }
  }
}
